//
//  DetailViewController.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 13/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var descriptionView: UILabel!
    
    var backgroundColor: UIColor?

    var selectedEnterprise: Enterprise? {
        didSet {
            if let id = selectedEnterprise?.id {
                getEnterprise(with: id)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let color = backgroundColor {
            containerView.backgroundColor = color
        }
        
        navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: - Data Manipulation
    
    func getEnterprise(with id: Int) {
        APIClient.getEnterprise(with: id) { result in
            switch result {
            case .success(let enterpriseResult):
                self.navigationItem.title = enterpriseResult.enterprise.enterpriseName
                self.name.text = enterpriseResult.enterprise.capitalizedName
                self.descriptionView.text = enterpriseResult.enterprise.enterpriseDescription
            case .failure(let error):
                print (error)
            }
        }
    }
}
