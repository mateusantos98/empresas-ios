//
//  Api.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import Foundation
import Alamofire

import Alamofire

class APIClient {
    static var customHeaders: [String:String]?
    
    private static var formattedHeaders: [String:String] {
        get {
            return APIClient.customHeaders ?? K.emptyDictionary
        }
    }
    
    @discardableResult
    private static func performRequest(route: APIRouter, completion: @escaping (AFDataResponse<Any>) -> Void) -> DataRequest {
        return AF.request(route)
            .validate(statusCode: 200..<300)
            .responseJSON() { response in
                completion(response)
        }
    }
    
    @discardableResult
    private static func performRequest<T: Decodable>(route: APIRouter, decoder: JSONDecoder = JSONDecoder(), completion: @escaping (Result<T, AFError>) -> Void) -> DataRequest {
        return AF.request(route)
            .validate(statusCode: 200..<300)
            .responseDecodable(decoder: decoder) { (response: DataResponse<T, AFError>) in
                completion(response.result)
        }
    }
    
    static func login(email: String, password: String, completion: @escaping (AFDataResponse<Any>) -> Void) {
        performRequest(route: APIRouter.login(email: email, password: password), completion: completion)
    }
    
    static func getEnterprisesByName(with name: String, completion: @escaping (Result<EnterprisesResult, AFError>) -> Void) {
        performRequest(route: APIRouter.enterprisesByName(customHeaders: APIClient.formattedHeaders, name: name), completion: completion)
    }
    
    static func getEnterprise(with id: Int, completion: @escaping (Result<EnterpriseResult, AFError>) -> Void) {
        performRequest(route: APIRouter.enterprise(customHeaders: APIClient.formattedHeaders, id: id), completion: completion)
    }
    
    static func getEnterprisePhoto(id: Int, path: String, completion: @escaping (AFDataResponse<Any>) -> Void) {
        performRequest(route: APIRouter.enterprisePhoto(customHeaders: APIClient.formattedHeaders, id: id, path: path), completion: completion)
    }
}
