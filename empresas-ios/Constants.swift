//
//  K.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import Foundation

struct K {
    static let emptyDictionary = ["": ""]
    static let cellNibName = "EnterpriseCell"
    static let cellIdentifier = "ReusableCell"
    static let primaryColorName = "PrimaryColor"
    static let lightGreyColorName = "LightGreyColor"
    
    struct Navigation {
        static let loginToEnterprisesSegue = "goToEnterprises"
        static let enterprisesToDetailSegue = "goToEnterprise"
    }
    
    struct Server {
        static let baseURL = "https://empresas.ioasys.com.br/api/v1"
    }
    
    struct APIParameterKey {
        static let email = "email"
        static let password = "password"
    }
}

enum HTTPHeaderField: String {
    case contentType = "Content-Type"
    case accessToken = "access-token"
    case client = "client"
    case uid = "uid"
    
}

enum ContentType: String {
    case json = "application/json"
}
