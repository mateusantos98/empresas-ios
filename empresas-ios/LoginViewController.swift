//
//  ViewController.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import UIKit
import Alamofire
import IQKeyboardManagerSwift

class LoginViewController: UIViewController {
    @IBOutlet weak var welcomeView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var emailTextField: CustomTextField!
    @IBOutlet weak var passwordTextField: CustomTextField!
    @IBOutlet weak var invalidCredentialsLabel: UILabel!
    
    @IBOutlet var formCenterYConstraint: NSLayoutConstraint!
    @IBOutlet var welcomeViewHeightConstraint: NSLayoutConstraint!
    
    var isEditingForm: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        passwordTextField.delegate = self
        
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.isHidden = true
    }
    
    //MARK: - TextField Actions
    
    @IBAction func onTextEditingDidBegin(_ sender: CustomTextField) {
        isEditingForm = true
        
        toggleEditingMode()
        
        untoggleTextFields()
        
        if sender == emailTextField {
            emailLabel.isHighlighted = true
        } else if sender == passwordTextField {
            passwordLabel.isHighlighted = true
        }
    }
    
    @IBAction func onTextEditingDigEnd(_ sender: CustomTextField) {
        isEditingForm = false
        
        toggleEditingMode()
        
        untoggleTextFields()
    }
    
    func untoggleTextFields() {
        emailLabel.isHighlighted = false
        passwordLabel.isHighlighted = false
    }
    
    //MARK: - Auth
    
    @IBAction func onLoginButtonPressed(_ sender: UIButton) {
        guard let email = emailTextField?.text, let password = passwordTextField?.text else {
            toggleCredentialsError()
            return
        }
        
        view.showLoading()
        
        APIClient.login(email: email, password: password) { response in
            switch response.result {
            case .success(_):
                guard let headerFields = response.response?.allHeaderFields else {
                    return
                }
                
                APIClient.customHeaders = ["access-token": headerFields["access-token"] as! String, "client": headerFields["client"] as! String, "uid": headerFields["uid"] as! String]
                
                self.performSegue(withIdentifier: K.Navigation.loginToEnterprisesSegue, sender: self)
                
                self.view.stopLoading()
            case .failure(let error):
                self.toggleCredentialsError()
                
                print(error.localizedDescription)
                
                self.view.stopLoading()
            }
        }
    }
    
    func toggleCredentialsError() {
        invalidCredentialsLabel.isHidden = false
        
        emailTextField.toggleErrorMode()
        passwordTextField.toggleErrorMode()
    }
}

//MARK: - Toggle Secure Text Entry Password

extension LoginViewController: DesignableTextFieldDelegate {
    func textFieldIconClicked(btn: UIButton) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
}

//MARK: - Orientation & Height Handler

extension LoginViewController {
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        // Better layout for the welcome view when in landscape mode
        coordinator.animate(alongsideTransition: nil) { _ in
            self.toggleEditingMode()
        }
    }
    
    func toggleEditingMode() {
        var shouldPushUp = true
        
        if UIDevice.current.orientation.isLandscape {
            self.welcomeViewHeightConstraint.constant = 50
            
            self.titleLabel.isHidden = true
        } else {
            welcomeViewHeightConstraint.constant = 150
            
            shouldPushUp = isEditingForm
            
            self.titleLabel.isHidden = isEditingForm
        }
        
        formCenterYConstraint.isActive = !shouldPushUp
        welcomeViewHeightConstraint.isActive = shouldPushUp
        
        view.layoutIfNeeded()
    }
}
