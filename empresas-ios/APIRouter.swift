//
//  ApiRouter.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import Alamofire

enum APIRouter: URLRequestConvertible {
    case login(email: String, password: String)
    case enterprisesByName(customHeaders: [String:String], name: String)
    case enterprise(customHeaders: [String:String], id: Int)
    case enterprisePhoto(customHeaders: [String:String], id: Int, path: String)
    
    // MARK: - HTTPMethod
    
    private var method: HTTPMethod {
        switch self {
        case .login:
            return .post
        case .enterprisesByName, .enterprise, .enterprisePhoto:
            return .get
        }
    }
    
    // MARK: - Path
    
    private var path: String {
        switch self {
        case .login:
            return "/users/auth/sign_in"
        case .enterprisesByName(_, let name):
            return "/enterprises?enterprise_types=1&name=\(name)"
        case .enterprise(_, let id):
            return "/enterprises/\(id)"
        case .enterprisePhoto(_, let id, let path):
            return "/uploads/enterprise/photo/\(id)/\(path)"
        }
    }
    
    // MARK: - Parameters
    
    private var parameters: Parameters? {
        switch self {
        case .login(let email, let password):
            return [K.APIParameterKey.email: email, K.APIParameterKey.password: password]
        case .enterprisesByName, .enterprise, .enterprisePhoto:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        let url = URL(string: K.Server.baseURL + path)!
        
        var urlRequest = URLRequest(url: url)
        
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.addValue(ContentType.json.rawValue, forHTTPHeaderField: HTTPHeaderField.contentType.rawValue)
        
        switch self {
        case .login:
            break
        case .enterprisesByName(let customHeaders, _), .enterprise(let customHeaders, _), .enterprisePhoto(let customHeaders, _, _):
            if let accessToken = customHeaders[HTTPHeaderField.accessToken.rawValue],
                let client = customHeaders[HTTPHeaderField.client.rawValue],
                let uid = customHeaders[HTTPHeaderField.uid.rawValue] {
                urlRequest.addValue(accessToken, forHTTPHeaderField: HTTPHeaderField.accessToken.rawValue)
                urlRequest.addValue(client, forHTTPHeaderField: HTTPHeaderField.client.rawValue)
                urlRequest.addValue(uid, forHTTPHeaderField: HTTPHeaderField.uid.rawValue)
            }
        }
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
