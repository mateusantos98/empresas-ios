//
//  Company.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import Foundation

// MARK: - Company
struct Enterprise: Codable {
    let id: Int
    let emailEnterprise, facebook, twitter, linkedin: String
    let phone: String
    let ownEnterprise: Bool
    let enterpriseName, photo, companyDescription, city: String
    let country: String
    let value, sharePrice: Int
    let enterpriseType: EnterpriseType

    enum CodingKeys: String, CodingKey {
        case id
        case emailEnterprise = "email_enterprise"
        case facebook, twitter, linkedin, phone
        case ownEnterprise = "own_enterprise"
        case enterpriseName = "enterprise_name"
        case photo
        case companyDescription = "description"
        case city, country, value
        case sharePrice = "share_price"
        case enterpriseType = "enterprise_type"
    }
}

// MARK: - EnterpriseType
struct EnterpriseType: Codable {
    let id: Int
    let enterpriseTypeName: String

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
}
