//
//  Company.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import UIKit

// MARK: - Result

struct EnterpriseResult: Codable {
    let enterprise: Enterprise
}

struct EnterprisesResult: Codable {
    let enterprises: [Enterprise]
}

// MARK: - Enterprise

struct Enterprise: Codable {
    let id, value, sharePrice: Int
    let emailEnterprise, facebook, twitter, linkedin, phone, photo: String?
    let ownEnterprise: Bool
    let enterpriseName, enterpriseDescription, city, country: String
    let enterpriseType: EnterpriseType
    
    var capitalizedName: String {
        get {
            return enterpriseName.uppercased()
        }
    }

    enum CodingKeys: String, CodingKey {
        case id
        case emailEnterprise = "email_enterprise"
        case facebook, twitter, linkedin, phone, photo
        case ownEnterprise = "own_enterprise"
        case enterpriseName = "enterprise_name"
        case enterpriseDescription = "description"
        case city, country, value
        case sharePrice = "share_price"
        case enterpriseType = "enterprise_type"
    }
}

// MARK: - EnterpriseType

struct EnterpriseType: Codable {
    let id: Int
    let enterpriseTypeName: String

    enum CodingKeys: String, CodingKey {
        case id
        case enterpriseTypeName = "enterprise_type_name"
    }
}
