//
//  CustomTextField.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 14/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import UIKit

protocol DesignableTextFieldDelegate: UITextFieldDelegate {
    func textFieldIconClicked(btn: UIButton)
}

@IBDesignable
class CustomTextField: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)
    
    let iconPadding: CGFloat = 15
    @IBInspectable var leadingImage: UIImage? { didSet { updateView() }}
    @IBInspectable var imageColor: UIColor = UIColor.systemRed { didSet { updateView() }}
    @IBInspectable var rtl: Bool = true { didSet { updateView() }  }
    
    private var myDelegate: DesignableTextFieldDelegate? {
        get { return delegate as? DesignableTextFieldDelegate }
    }

    @objc func buttonClicked(btn: UIButton){
        myDelegate?.textFieldIconClicked(btn: btn)
    }
    
    override func draw(_ rect: CGRect) {
        layer.cornerRadius = 10
        clipsToBounds = true
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        if rtl {
            return bounds.inset(by: padding)
        }
        
        return super.textRect(forBounds: bounds)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        if rtl {
            return bounds.inset(by: padding)
        }
        
        return super.placeholderRect(forBounds: bounds)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        if rtl {
            return bounds.inset(by: padding)
        }
        
        return super.editingRect(forBounds: bounds)
    }
    
    //Padding images on left
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += iconPadding
        return textRect
    }

    //Padding images on Right
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= iconPadding
        return textRect
    }

    func updateView() {
        rightViewMode = UITextField.ViewMode.never
        rightView = nil
        leftViewMode = UITextField.ViewMode.never
        leftView = nil

        if let image = leadingImage {
            let button = UIButton(type: .custom)

            let tintedImage = image.withRenderingMode(.alwaysTemplate)
            button.setImage(tintedImage, for: .normal)
            button.tintColor = imageColor

            button.setTitleColor(UIColor.clear, for: .normal)
            button.addTarget(self, action: #selector(buttonClicked(btn:)), for: UIControl.Event.touchDown)
            button.isUserInteractionEnabled = true

            if rtl {
                button.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
                
                rightViewMode = UITextField.ViewMode.always
                rightView = button
            } else {
                button.frame = CGRect(x: 10, y: 5, width: 20, height: 20)
                
                let buttonContainer: UIView = UIView(frame:
                               CGRect(x: 20, y: 0, width: 40, height: 30))
                
                buttonContainer.addSubview(button)
                
                leftViewMode = UITextField.ViewMode.always
                leftView = buttonContainer
            }
        }
    }
    
    func toggleErrorMode() {
        leadingImage = UIImage.init(systemName: "xmark.circle.fill")
        imageColor = UIColor.systemRed
        rtl = true
        
        layer.borderColor = UIColor.systemRed.cgColor

        layer.borderWidth = 1.0
    }
}
