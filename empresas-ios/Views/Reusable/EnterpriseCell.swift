//
//  EnterpriseCell.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import UIKit

class EnterpriseCell: UITableViewCell {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
