//
//  SpinnerViewController.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 14/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import UIKit
import SwiftSpinner

extension UIView {
    static let loadingViewTag = 1938123987
    
    func showLoading(showBlurBackground: Bool = true) {
        let color = #colorLiteral(red: 0.9843137255, green: 0.8588235294, blue: 0.9058823529, alpha: 1)
        SwiftSpinner.shared.innerColor = color
        SwiftSpinner.shared.outerColor = color
        
        SwiftSpinner.showBlurBackground = showBlurBackground
        SwiftSpinner.show("")
    }
    
    func stopLoading() {
        SwiftSpinner.hide()
    }
}
