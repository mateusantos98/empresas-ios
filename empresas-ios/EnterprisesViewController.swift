//
//  TableTableViewController.swift
//  empresas-ios
//
//  Created by mateus henrique lino santos on 12/08/20.
//  Copyright © 2020 Mateus Santos. All rights reserved.
//

import UIKit
import Alamofire
import IQKeyboardManagerSwift

class EnterprisesViewController: UIViewController {
    @IBOutlet var initialBackgroundView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: CustomTextField!
    @IBOutlet weak var initialBackgroundHeightConstraint: NSLayoutConstraint!
    
    
    private var enterprises: [Enterprise]?
    
    private var selectedRow: Int {
        get {
            guard let selected = tableView.indexPathForSelectedRow?.row else {
                return -1
            }
            
            return selected - 1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTextField.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: K.cellNibName, bundle: nil), forCellReuseIdentifier: K.cellIdentifier)
        
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
    }
    
    
    //MARK: - Data Manipulation
    
    func getEnterprisesByName(with name: String) {
        view.showLoading(showBlurBackground: false)
        
        APIClient.getEnterprisesByName(with: name) { result in
            switch result {
            case .success(let enterprisesResult):
                self.enterprises = enterprisesResult.enterprises
                
                self.tableView.reloadData()
                
                self.view.stopLoading()
            case .failure(let error):
                print (error)
                
                self.view.stopLoading()
            }
        }
    }
}


// MARK: - TableView Data Source Methods

extension EnterprisesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let enterprisesCount = enterprises?.count {
            return enterprisesCount + 1
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0, let safeEnterprises = enterprises {
            let cell = UITableViewCell()
            
            cell.selectionStyle = .none
            
            var fontSize: CGFloat = 18
            
            if safeEnterprises.count == 0 {
                cell.textLabel?.text = "Nenhum resultado encontrado"
                
                cell.textLabel?.textAlignment = .center
                
            } else {
                cell.textLabel?.text = "\(safeEnterprises.count) resultados encontrados"
                
                fontSize = 14
            }
            
            cell.textLabel?.font = UIFont.systemFont(ofSize: fontSize, weight: .thin)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: K.cellIdentifier, for: indexPath) as! EnterpriseCell
        
        if let enterprise = enterprises?[indexPath.row - 1] {
            var color = cell.containerView.backgroundColor
            
            cell.nameLabel.text = enterprise.capitalizedName
            
            if color == nil {
                color = generateRandomPastelColor(withMixedColor: UIColor.init(named: K.primaryColorName))
            }
            
            cell.containerView.backgroundColor = color
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let safeEnterprises = enterprises {
            if safeEnterprises.count > 0 {
                if indexPath.row == 0 {
                    return 50
                }
                
                return 150
            }
        }
        
        return tableView.frame.height
    }
}

//MARK: - TableView Delegate Methods

extension EnterprisesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.cellForRow(at: indexPath) is EnterpriseCell {
            performSegue(withIdentifier: K.Navigation.enterprisesToDetailSegue, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC = segue.destination as! DetailViewController
        
        guard let currentEnterprise = enterprises?[selectedRow] else {
            print("No enterprise selected")
            
            return
        }
        
        destinationVC.backgroundColor = (tableView.cellForRow(at: tableView.indexPathForSelectedRow!)! as! EnterpriseCell).containerView.backgroundColor!
        
        destinationVC.selectedEnterprise = currentEnterprise
    }
}

//MARK: - SearchBar Delegate Methods

extension EnterprisesViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        initialBackgroundHeightConstraint.constant /= 2
        
        toggleInitialBackground()
        
        view.layoutIfNeeded()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        initialBackgroundHeightConstraint.constant *= 2
        
        toggleInitialBackground()
        
        view.layoutIfNeeded()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        enterprises = []
        
        if let text = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) {
            if text.count > 0 {
                getEnterprisesByName(with: text)
            }
        }
        
        IQKeyboardManager.shared.resignFirstResponder()
        
        tableView.reloadData()
        
        return true
    }
    
    func toggleInitialBackground() {
        for subView in initialBackgroundView.subviews {
            subView.isHidden = !subView.isHidden
        }
    }
}

//MARK: - Cell Color Methods

extension EnterprisesViewController {
    func generateRandomPastelColor(withMixedColor mixColor: UIColor?) -> UIColor {
        let randomColorGenerator = { ()-> CGFloat in
            CGFloat(arc4random() % 256 ) / 256
        }
            
        var red: CGFloat = randomColorGenerator()
        var green: CGFloat = randomColorGenerator()
        var blue: CGFloat = randomColorGenerator()
            
        if let mixColor = mixColor {
            var mixRed: CGFloat = 0, mixGreen: CGFloat = 0, mixBlue: CGFloat = 0;
            mixColor.getRed(&mixRed, green: &mixGreen, blue: &mixBlue, alpha: nil)
            
            red = (red + mixRed) / 2;
            green = (green + mixGreen) / 2;
            blue = (blue + mixBlue) / 2;
        }
            
        return UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
}
